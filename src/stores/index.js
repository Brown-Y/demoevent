import { createPinia } from 'pinia'
import persist from 'pinia-plugin-persistedstate'

const pinia = createPinia()
pinia.use(persist)

export default pinia
export * from './modules/user'
/**
 * 这里面export * from '模块'这行代码相当于
 * import {useUserStore} from '模块'
 * export {useUserStore}这两行代码，这里实现了仓库的统一管理
 */
