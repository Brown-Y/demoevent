import { defineStore } from 'pinia'
import { ref } from 'vue'
import { getUserInfo } from '@/api/user'
//用户模块
export const useUserStore = defineStore(
  'user',
  () => {
    const token = ref('')
    const setToken = (value) => {
      token.value = value
    }
    const removeInfo = () => {
      setToken('')
      userInfo.value = {}
    }
    //获取用户信息
    const userInfo = ref({})
    const getUserInfoAction = async () => {
      const res = await getUserInfo()
      userInfo.value = res.data
    }
    return {
      token,
      setToken,
      userInfo,
      getUserInfoAction,
      removeInfo
    }
  },
  { persist: true }
)
