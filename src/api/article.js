import request from '@/utils/request'
// 文章分类
// 获取文章分类列表
export function getArticleListService() {
  return request({
    url: '/my/cate/list'
  })
}
// 增加文章分类
export const addArticleCateService = (data) =>
  request.post('/my/cate/add', data)
//获取文章分类详情
export const getArticleCateDetailService = (id) => {
  return request.get('/my/cate/info', { params: { id } })
}
// 编辑文章分类
export const editArticleCateService = (data) => {
  return request.put('/my/cate/info', data)
}
// 删除文章分类
export const deleteArticleCateService = (id) => {
  return request({
    url: '/my/cate/del',
    method: 'delete',
    params: { id }
  })
}
// 文章管理页面
// 获取文章列表
export const getArticleManageListService = (params) => {
  return request.get('/my/article/list', { params })
}
// 发布文章
export const addArticleManageService = (data) => {
  return request.post('/my/article/add', data)
}
// 获取文章详情
export const getArticleManageDetailService = (id) => {
  return request.get('/my/article/info', { params: { id } })
}
// 修改文章
export const editArticleManageService = (data) => {
  return request.put('/my/article/info', data)
}
// 删除文章
export const deleteArticleManageService = (id) => {
  return request.delete('/my/article/info', { params: { id } })
}
