import request from '@/utils/request'
// 获取用户信息
export function getUserInfo() {
  return request({
    url: '/my/userinfo'
  })
}
// 修改用户信息
export const updateUserInfo = (data) => {
  return request({
    url: '/my/userinfo',
    method: 'put',
    data
  })
}
// 修改用户头像
export const updateUserAvatar = (avatar) => {
  return request({
    url: '/my/update/avatar',
    method: 'patch',
    data: { avatar }
  })
}
/* export const updateUserAvatar = (avatar) => {
  return request.patch('/my/update/avatar', { avatar })
} */
// 修改用户密码
export const updateUserPwd = (data) => {
  return request.patch('/my/updatepwd', data)
}
