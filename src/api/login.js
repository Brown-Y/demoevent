import request from '@/utils/request'

//注册
export function register(data) {
  return request({
    url: '/api/reg',
    method: 'post',
    data
  })
}
//登录
export function loginService(data) {
  return request({
    url: '/api/login',
    method: 'post',
    data
  })
}
